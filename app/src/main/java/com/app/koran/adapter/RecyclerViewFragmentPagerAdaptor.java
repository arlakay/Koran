package com.app.koran.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.app.koran.fragment.FragmentCategoryDetail;
import com.app.koran.model.Category;

import java.util.List;

/**
 * Adaptor for ViewPager
 */
public class RecyclerViewFragmentPagerAdaptor extends FragmentPagerAdapter {
    private List<Category> categories;

    public RecyclerViewFragmentPagerAdaptor(FragmentManager fm, List<Category> categories) {
        super(fm);
        this.categories = categories;
    }

    @Override
    public Fragment getItem(int position) {
        return FragmentCategoryDetail.newInstance(categories.get(position).id);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return categories.get(position).title;
    }

    @Override
    public int getCount() {
        return categories.size();
    }
}
