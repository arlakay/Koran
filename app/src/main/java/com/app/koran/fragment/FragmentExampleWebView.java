package com.app.koran.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.app.koran.ActivityMain;
import com.app.koran.R;

public class FragmentExampleWebView extends Fragment {
    private View root_view, parent_view;
    private WebView webView;

    public FragmentExampleWebView() {

    }

    public static FragmentExampleWebView newInstance() {
        FragmentExampleWebView fragment = new FragmentExampleWebView();
        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
//            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        root_view = inflater.inflate(R.layout.fragment_fragment_example_web_view, null);
        parent_view = getActivity().findViewById(R.id.main_content);

        webView = (WebView) root_view.findViewById(R.id.webView);

        String urlGoogle = "http://pradipta.id/";
        loadWebFromUrl(urlGoogle);

        return root_view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.action_search);
        item.setVisible(false);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private void loadWebFromUrl(String url) {
        webView.loadUrl("about:blank");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings();
        webView.loadUrl(url);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                ((ActivityMain) getActivity()).getSupportActionBar().setTitle( getString(R.string.webview_loading) + progress + " %");
                if (progress == 100) {
                    ((ActivityMain) getActivity()).getSupportActionBar().setTitle(R.string.title_nav_example);
                }
            }
        });
    }
}
