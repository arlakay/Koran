package com.app.koran.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.app.koran.ActivityMain;
import com.app.koran.ActivityPostDetails;
import com.app.koran.R;
import com.app.koran.adapter.AdapterPostList;
import com.app.koran.adapter.RecyclerViewFragmentPagerAdaptor;
import com.app.koran.connection.API;
import com.app.koran.connection.RestAdapter;
import com.app.koran.connection.callbacks.CallbackCategories;
import com.app.koran.connection.callbacks.CallbackListPost;
import com.app.koran.data.Constant;
import com.app.koran.model.Category;
import com.app.koran.model.Post;
import com.app.koran.utils.NetworkCheck;
import com.app.koran.utils.Tools;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FragmentHome extends Fragment {
    private View root_view, parent_view;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Call<CallbackCategories> callbackCall = null;
    protected static List<Category> categories = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_home, null);
        parent_view = getActivity().findViewById(R.id.main_content);

        viewPager = (ViewPager) root_view.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) root_view.findViewById(R.id.tabs);

        // get enabled controllers
        Tools.requestInfoApi(getActivity());

        requestAction();

        return root_view;
    }

    private void displayApiResult(final List<Category> categoryList) {
//        mAdapter.setListData(categories);

        categories = categoryList;
        RecyclerViewFragmentPagerAdaptor adaptor = new
                RecyclerViewFragmentPagerAdaptor(getChildFragmentManager(), categoryList);
        viewPager.setAdapter(adaptor);
        tabLayout.setupWithViewPager(viewPager);

        if (categories.size() == 0) {
            showNoItemView(true);
        }
    }

    private void requestCategoriesApi() {
        API api = RestAdapter.createAPI();
        callbackCall = api.getAllCategories();
        callbackCall.enqueue(new Callback<CallbackCategories>() {
            @Override
            public void onResponse(Call<CallbackCategories> call, Response<CallbackCategories> response) {
                CallbackCategories resp = response.body();
                if (resp != null && resp.status.equals("ok")) {
                    displayApiResult(resp.categories);
                } else {
                    onFailRequest();
                }
            }

            @Override
            public void onFailure(Call<CallbackCategories> call, Throwable t) {
                if (!call.isCanceled()) onFailRequest();
            }

        });
    }

    private void onFailRequest() {
        if (NetworkCheck.isConnect(getActivity())) {
            showFailedView(true, getString(R.string.failed_text));
        } else {
            showFailedView(true, getString(R.string.no_internet_text));
        }
    }

    private void requestAction() {
        showFailedView(false, "");
        showNoItemView(false);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                requestCategoriesApi();
            }
        }, Constant.DELAY_TIME);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(callbackCall != null && callbackCall.isExecuted()){
            callbackCall.cancel();
        }
    }

    private void showFailedView(boolean flag, String message) {
        View lyt_failed = (View) root_view.findViewById(R.id.lyt_failed_category);
        ((TextView) root_view.findViewById(R.id.failed_message)).setText(message);
        if (flag) {
            lyt_failed.setVisibility(View.VISIBLE);
        } else {
            lyt_failed.setVisibility(View.GONE);
        }
        ((Button) root_view.findViewById(R.id.failed_retry)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                requestAction();
            }
        });
    }

    private void showNoItemView(boolean show) {
        View lyt_no_item = (View) root_view.findViewById(R.id.lyt_no_item_category);
        ((TextView) root_view.findViewById(R.id.no_item_message)).setText(R.string.no_category);
        if (show) {
            lyt_no_item.setVisibility(View.VISIBLE);
        } else {
            lyt_no_item.setVisibility(View.GONE);
        }
    }

}

