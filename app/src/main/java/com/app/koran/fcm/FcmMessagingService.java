package com.app.koran.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;

import com.app.koran.ActivityMain;
import com.app.koran.ActivityPostDetails;
import com.app.koran.R;
import com.app.koran.data.SharedPref;
import com.app.koran.model.FcmNotif;
import com.app.koran.model.Post;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class FcmMessagingService extends FirebaseMessagingService {
    private static final String CHANNEL_ID = "Koran100";
    private static final CharSequence CHANNEL_NAME = "News";
    private static final int NOTIFICATION_ID = 6501;
    private static int VIBRATION_TIME = 500; // in millisecond
    private SharedPref sharedPref;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sharedPref = new SharedPref(this);
        if (sharedPref.getNotification()) {
            // play vibration
            if (sharedPref.getVibration()) {
                ((Vibrator) getSystemService(Context.VIBRATOR_SERVICE)).vibrate(VIBRATION_TIME);
            }
            RingtoneManager.getRingtone(this, Uri.parse(sharedPref.getRingtone())).play();

            if (remoteMessage.getData().size() > 0) {
                Map<String, String> data = remoteMessage.getData();
                FcmNotif fcmNotif = new FcmNotif();
                fcmNotif.setTitle(data.get("title"));
                fcmNotif.setContent(data.get("content"));
                fcmNotif.setPost_id(Integer.parseInt(data.get("post_id")));
//                displayNotificationIntent(fcmNotif);

//                if (fcmNotif.getPost_id() != -1) {
//                    Post post = new Post();
//                    post.title = fcmNotif.getTitle();
//                    post.id = fcmNotif.getPost_id();

                    Post a = new Post();
                    a.id = Integer.parseInt( remoteMessage.getData().get("post_id") );
                    a.content = remoteMessage.getData().get("content");

                    AppNotificationManager mAppNotificationManager = new AppNotificationManager(FcmMessagingService.this);
                    mAppNotificationManager.showDetailsNotificationWithAllCitiesAction(a);
//                }
            }
        }
    }

    private void displayNotificationIntent(FcmNotif fcmNotif) {

        Intent intent = new Intent(this, ActivityPostDetails.class);
        if (fcmNotif.getPost_id() != -1) {
            intent = new Intent(this, ActivityPostDetails.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            Post post = new Post();
            post.title = fcmNotif.getTitle();
            post.id = fcmNotif.getPost_id();
            boolean from_notif = !ActivityMain.active;
            intent.putExtra(ActivityPostDetails.EXTRA_OBJC, post);
            intent.putExtra(ActivityPostDetails.EXTRA_NOTIF, from_notif);
        }

        PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intent, 0);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.drawable.ic_notification);
        builder.setContentTitle(fcmNotif.getTitle());
        builder.setContentText(fcmNotif.getContent());
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(fcmNotif.getContent()));
        builder.setDefaults(Notification.DEFAULT_LIGHTS);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            builder.setPriority(Notification.PRIORITY_HIGH);
        }

        builder.setContentIntent(pendingIntent);
        builder.setAutoCancel(false);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new
                    NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableVibration(true);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.YELLOW);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(notificationChannel);
        }

        int unique_id = (int) System.currentTimeMillis();
        notificationManager.notify(unique_id, builder.build());
    }

}
